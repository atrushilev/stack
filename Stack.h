#include <iostream>
#include <cassert>
using namespace std; 

class Stack
{
private:
    int m_length = 0;
    int* m_data = nullptr;
    int* top_ = nullptr;
public:
    Stack() : m_length(0), m_data(nullptr), top_(nullptr)
    {}
    Stack(int length)
    {
        assert(length >= 0);
        m_data = new int[length];
        top_ = m_data;
        m_length = length;
    }

    ~Stack()
    {
        delete[] m_data;
    }

    void push(int value)
    {
        int used = top_ - m_data;
        if (used >= m_length) {
            int* data = new int[used + 1];
            memcpy(data, m_data, used * sizeof(int));
            top_ = data + used;
            m_length = used + 1;
            delete[] m_data;
            m_data = data;
        }
        *top_ = value;
        ++top_;
    }

    int pop()
    {
        assert(m_length > 0);
        int top = m_data[m_length - 1];
        --m_length;
        return top;
    }

    void printStack()
    {
        for (int i = 0; i < m_length; i++)
            cout << m_data[i] << ' ';
    }

    int showTop()
    {
        int top = m_data[m_length - 1];
        return top;
    }
};
