#include <iostream>
#include <cassert>
#include "Stack.h"
using namespace std;

int main()
{

    int n;
    int i = 0;
    Stack stek;
    cout << "Enter the size of your stack: ";
    cin >> n;
    cout << "Enter " << n << " number(s): ";
    while (i != n)
    {
        int a;
        cin >> a;
        stek.push(a);
        i++;
    }
    cout << "Initial stack: "; stek.printStack();
    cout << "\nTop element: " << stek.showTop();
    cout << "\nPush element..."; stek.push(20);
    cout << "\nUpdated stack: "; stek.printStack();
    cout << "\nPop element... "; stek.pop();
    cout << "\nPop element... "; stek.pop();
    cout << "\nTop element: " << stek.showTop();
    cout << "\nPop element: "; stek.pop();
    cout << "\nFinal stack: "; stek.printStack();
    cout << endl;

    return 0;
}
